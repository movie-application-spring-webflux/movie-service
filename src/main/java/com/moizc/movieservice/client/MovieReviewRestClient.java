package com.moizc.movieservice.client;

import com.moizc.movieservice.dto.MovieReviewDto;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Flux;

@Component
public class MovieReviewRestClient {

    private final WebClient webClient;

    @Value("${rest-client.movie-review-url}")
    private String movieReviewUrl;

    public MovieReviewRestClient(WebClient webClient) {
        this.webClient = webClient;
    }

    public Flux<MovieReviewDto> retrieveMovieReviewDto(String movieInfoId) {
        return webClient
                .get()
                .uri(movieReviewUrl + "/movie-reviews/{movie_info_id}", movieInfoId)
                .retrieve()
                .bodyToFlux(MovieReviewDto.class)
                .log();
    }

}
