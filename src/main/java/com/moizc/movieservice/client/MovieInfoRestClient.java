package com.moizc.movieservice.client;

import com.moizc.movieservice.dto.MovieInfoDto;
import com.moizc.movieservice.exception.MovieInfoClientException;
import com.moizc.movieservice.exception.MovieInfoServerException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.HttpStatusCode;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.Exceptions;
import reactor.core.publisher.Mono;
import reactor.util.retry.Retry;
import reactor.util.retry.RetryBackoffSpec;

import java.time.Duration;

@Component
@Slf4j
public class MovieInfoRestClient {

    private final WebClient webClient;

    @Value("${rest-client.movie-info-url}")
    private String movieInfoUrl;

    public MovieInfoRestClient(WebClient webClient) {
        this.webClient = webClient;
    }

    public Mono<MovieInfoDto> retrieveMovieInfoDto(String movieInfoId) {

        RetryBackoffSpec backoffSpec = Retry.fixedDelay(3, Duration.ofSeconds(1))
                .filter(exception -> exception instanceof MovieInfoServerException)
                .onRetryExhaustedThrow((retryBackoffSpec, retrySignal) -> Exceptions.propagate(retrySignal.failure()));

        return webClient
                .get()
                .uri(movieInfoUrl + "/movie-info/id/{movie_id}", movieInfoId)
                .retrieve()
                .onStatus(HttpStatusCode::is4xxClientError, clientResponse -> {
                    if (clientResponse.statusCode().equals(HttpStatus.NOT_FOUND)) {
                        return Mono
                                .error(MovieInfoClientException.builder()
                                        .errorMessage("Movie Info not found")
                                        .errorStatus(HttpStatus.NOT_FOUND)
                                        .build()
                                );
                    }

                    return clientResponse.bodyToMono(String.class)
                            .flatMap(response -> Mono.error(MovieInfoClientException.builder()
                                    .errorMessage(response)
                                    .errorStatus(HttpStatus.valueOf(clientResponse.statusCode().value()))
                                    .build()
                            ));
                })
                .onStatus(HttpStatusCode::is5xxServerError, clientResponse -> Mono.error(MovieInfoServerException.builder()
                                .errorMessage("Service Unavailable")
                        .errorStatus(HttpStatus.INTERNAL_SERVER_ERROR)
                        .build()))
                .bodyToMono(MovieInfoDto.class)
                .retryWhen(backoffSpec)
                .log();
    }
}
