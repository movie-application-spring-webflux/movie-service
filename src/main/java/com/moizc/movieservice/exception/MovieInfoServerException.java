package com.moizc.movieservice.exception;

import lombok.Builder;
import lombok.Getter;
import org.springframework.http.HttpStatus;

@Builder
@Getter
public class MovieInfoServerException extends RuntimeException {

    private final String errorMessage;
    private final HttpStatus errorStatus;
    public MovieInfoServerException(String errorMessage, HttpStatus errorStatus) {
        super(errorMessage);
        this.errorMessage = errorMessage;
        this.errorStatus = errorStatus;
    }
}
