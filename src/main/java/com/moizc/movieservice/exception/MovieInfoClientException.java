package com.moizc.movieservice.exception;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import org.springframework.http.HttpStatus;

@Getter
@Setter
@Builder
public class MovieInfoClientException extends RuntimeException {

    private final String errorMessage;
    private final HttpStatus errorStatus;

    public MovieInfoClientException(String errorMessage, HttpStatus errorStatus) {
        super(errorMessage);
        this.errorMessage = errorMessage;
        this.errorStatus = errorStatus;
    }
}
