package com.moizc.movieservice.exception;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
@Slf4j
public class ErrorHandler {

    @ExceptionHandler(MovieInfoClientException.class)
    public ResponseEntity<String> handleClientException(MovieInfoClientException movieInfoClientException) {
        log.error("Exception: ", movieInfoClientException);
        return ResponseEntity.status(movieInfoClientException.getErrorStatus()).body(movieInfoClientException.getErrorMessage());
    }

    @ExceptionHandler(MovieInfoServerException.class)
    public ResponseEntity<String> handleServerException(MovieInfoServerException movieInfoServerException) {
        log.error("Exception: ", movieInfoServerException);
        return ResponseEntity.status(movieInfoServerException.getErrorStatus()).body(movieInfoServerException.getErrorMessage());
    }

    @ExceptionHandler(RuntimeException.class)
    public ResponseEntity<String> handleRuntimeException(RuntimeException runtimeException) {
        log.error("Exception: ", runtimeException);
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Something went wrong :(");
    }
}
