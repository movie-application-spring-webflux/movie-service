package com.moizc.movieservice.model;

import com.moizc.movieservice.dto.MovieInfoDto;
import com.moizc.movieservice.dto.MovieReviewDto;
import lombok.Builder;
import lombok.Data;

import java.util.List;

@Builder
@Data
public class Movie {
    private MovieInfoDto movieInfoDto;
    private List<MovieReviewDto> movieReviewDtos;
}
