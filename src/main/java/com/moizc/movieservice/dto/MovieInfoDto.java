package com.moizc.movieservice.dto;

import lombok.Builder;

import java.time.LocalDate;
import java.util.List;

@Builder
public record MovieInfoDto(
        String name,
        List<String> cast,
        Integer releaseYear,
        LocalDate releaseDate) {
}
