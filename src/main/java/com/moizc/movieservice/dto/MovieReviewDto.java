package com.moizc.movieservice.dto;

import lombok.Builder;

@Builder
public record MovieReviewDto(
        String movieInfoId,
        String comment,
        Double rating,
        String reviewerId) {
}
