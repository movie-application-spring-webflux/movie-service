package com.moizc.movieservice.controller;

import com.moizc.movieservice.client.MovieInfoRestClient;
import com.moizc.movieservice.client.MovieReviewRestClient;
import com.moizc.movieservice.dto.MovieReviewDto;
import com.moizc.movieservice.model.Movie;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;

import java.util.List;

@RestController
@RequestMapping("/v1")
public class MovieController {

    private final MovieInfoRestClient movieInfoRestClient;
    private final MovieReviewRestClient movieReviewRestClient;

    public MovieController(MovieInfoRestClient movieInfoRestClient, MovieReviewRestClient movieReviewRestClient) {
        this.movieInfoRestClient = movieInfoRestClient;
        this.movieReviewRestClient = movieReviewRestClient;
    }

    @GetMapping("/movie/{movie_info_id}")
    public Mono<Movie> getMovieById(@PathVariable String movie_info_id) {
        return movieInfoRestClient.retrieveMovieInfoDto(movie_info_id)
                .flatMap(movieInfoDto -> {
                    Mono<List<MovieReviewDto>> reviews = movieReviewRestClient.retrieveMovieReviewDto(movie_info_id).collectList();

                    return reviews.map(review -> Movie.builder()
                            .movieInfoDto(movieInfoDto)
                            .movieReviewDtos(review)
                            .build());
                });
    }
}
