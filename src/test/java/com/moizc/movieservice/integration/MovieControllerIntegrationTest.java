package com.moizc.movieservice.integration;

import com.github.tomakehurst.wiremock.client.WireMock;
import com.moizc.movieservice.model.Movie;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebTestClient;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.cloud.contract.wiremock.AutoConfigureWireMock;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.web.reactive.server.WebTestClient;

import java.time.LocalDate;
import java.util.List;

import static com.github.tomakehurst.wiremock.client.WireMock.*;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureWebTestClient
@AutoConfigureWireMock(port = 8084)
@TestPropertySource(
        properties = {
                "rest-client.movie-info-url=http://localhost:8084/v1",
                "rest-client.movie-review-url=http://localhost:8084/v1"
        }
)
public class MovieControllerIntegrationTest {

    @Autowired
    private WebTestClient webTestClient;

    @Test
    void shouldRetrieveMovieInfoAndReviewsForTheGivenMovieInfoId() {

        String movieInfoId = "1fa";

        stubFor(get(urlEqualTo("/v1/movie-info/id/" + movieInfoId))
                .willReturn(aResponse()
                        .withHeader("Content-Type", "application/json")
                        .withBodyFile("movieInfo.json")));

        stubFor(get(urlEqualTo("/v1/movie-reviews/" + movieInfoId))
                .willReturn(aResponse()
                        .withHeader("Content-Type", "application/json")
                        .withBodyFile("movieReviews.json")));

        webTestClient
                .get()
                .uri("/v1/movie/" + movieInfoId)
                .exchange()
                .expectStatus()
                .isOk()
                .expectBody(Movie.class)
                .consumeWith(movieEntityExchangeResult -> {
                    Movie responseBody = movieEntityExchangeResult.getResponseBody();
                    assert responseBody != null;
                    Assertions.assertEquals(2, responseBody.getMovieReviewDtos().size());
                    Assertions.assertEquals("Avengers", responseBody.getMovieInfoDto().name());
                    Assertions.assertEquals(List.of("RDJ", "Chris Evans"), responseBody.getMovieInfoDto().cast());
                    Assertions.assertEquals(2010, responseBody.getMovieInfoDto().releaseYear());
                    Assertions.assertEquals(LocalDate.of(2010, 2, 20), responseBody.getMovieInfoDto().releaseDate());
                    Assertions.assertEquals("nice", responseBody.getMovieReviewDtos().get(0).comment());
                    Assertions.assertEquals(4.0, responseBody.getMovieReviewDtos().get(0).rating());
                    Assertions.assertEquals("pqr", responseBody.getMovieReviewDtos().get(0).reviewerId());
                    Assertions.assertEquals("amazing", responseBody.getMovieReviewDtos().get(1).comment());
                    Assertions.assertEquals(4.8, responseBody.getMovieReviewDtos().get(1).rating());
                    Assertions.assertEquals("abc", responseBody.getMovieReviewDtos().get(1).reviewerId());
                });
    }

    @Test
    void shouldThrowNotFoundErrorWhenMovieInfoIdIsNotPresentInDatabase() {

        String movieInfoId = "1fa";

        stubFor(get(urlEqualTo("/v1/movie-info/id/" + movieInfoId))
                .willReturn(aResponse()
                        .withStatus(404)));

        webTestClient
                .get()
                .uri("/v1/movie/" + movieInfoId)
                .exchange()
                .expectStatus()
                .is4xxClientError()
                .expectBody(String.class)
                .isEqualTo("Movie Info not found");

        WireMock.verify(1, getRequestedFor(urlEqualTo("/v1/movie-info/id/" + movieInfoId)));
    }

    @Test
    void shouldThrow5xxErrorWhenSomethingGoesWrongInMovieInfoService() {

        String movieInfoId = "1fa";

        stubFor(get(urlEqualTo("/v1/movie-info/id/" + movieInfoId))
                .willReturn(aResponse()
                        .withStatus(500)));

        webTestClient
                .get()
                .uri("/v1/movie/" + movieInfoId)
                .exchange()
                .expectStatus()
                .is5xxServerError()
                .expectBody(String.class)
                .isEqualTo("Service Unavailable");

        WireMock.verify(4, getRequestedFor(urlEqualTo("/v1/movie-info/id/" + movieInfoId)));
    }
}
